# misc-transcriptomes

## hemichordates ##
mostly from [Cannon et al 2014](http://datadryad.org/resource/doi:10.5061/dryad.20s7c) datasets, in [Phylogenomic Resolution of the Hemichordate and Echinoderm Clade](http://www.sciencedirect.com/science/article/pii/S0960982214012925)

* [Cephalodiscus gracilis](https://bitbucket.org/wrf/misc-transcriptomes/downloads/cephalodiscus_gracilis_trinity.renamed.fasta.gz) : [SRR1695473](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695473)
* [Harrimaniidae Iceland](https://bitbucket.org/wrf/misc-transcriptomes/downloads/harrimaniidae_Iceland_trinity.renamed.fasta.gz) : [SRR1695462](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695462)
* [Harrimaniidae Norway](https://bitbucket.org/wrf/misc-transcriptomes/downloads/harrimaniidae_Norway_trinity.renamed.fasta.gz) : [SRR1695463](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695463)
* [Ptychodera bahamensis](https://bitbucket.org/wrf/misc-transcriptomes/downloads/ptychodera_bahamensis_trinity.renamed.fasta.gz) : [SRR1695458](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695458)
* [Rhabdopleura sp](https://bitbucket.org/wrf/misc-transcriptomes/downloads/rhabdopleura_trinity.renamed.fasta.gz) : [SRR1806842](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1806842)
* [Torquaratoridae sp Antarctica](https://bitbucket.org/wrf/misc-transcriptomes/downloads/torquaratoridae_Antarctica_trinity.renamed.fasta.gz) : [SRR1695469](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695469)
* [Torquaratoridae sp Iceland](https://bitbucket.org/wrf/misc-transcriptomes/downloads/torquaratoridae_Iceland_trinity.renamed.fasta.gz) : [SRR1695468](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695468)

![cannon2014_299-33_percent_matrix.barplot.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/cannon2014_299-33_percent_matrix.barplot.png)

## echinoderms ##
as above, from [Cannon et al 2014](http://datadryad.org/resource/doi:10.5061/dryad.20s7c)

* [Astrotoma agssizii](https://bitbucket.org/wrf/misc-transcriptomes/downloads/astrotoma_agssizii_trinity.renamed.fasta.gz) : [SRR1695485](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695485)
* [Dumetocrinus antarcticus](https://bitbucket.org/wrf/misc-transcriptomes/downloads/dumetocrinus_antarcticus_trinity.renamed.fasta.gz) : [SRR5564112](https://www.ncbi.nlm.nih.gov/sra/?term=SRR5564112)
* [Notocrinus virillis](https://bitbucket.org/wrf/misc-transcriptomes/downloads/notocrinus_virillis_trinity.renamed.fasta.gz) : [SRR5564113](https://www.ncbi.nlm.nih.gov/sra/?term=SRR5564113)
* [Parastichopus californicus](https://bitbucket.org/wrf/misc-transcriptomes/downloads/parastichopus_californicus_trinity.renamed.fasta.gz) : [SRR1695477](https://www.ncbi.nlm.nih.gov/sra/?term=SRR1695477)

## arthropods ##
* [Luciola aquatilis](https://bitbucket.org/wrf/misc-transcriptomes/downloads/luciola_aquatilis_trinity.fasta.gz) : [SRR3195455](https://www.ncbi.nlm.nih.gov/sra/SRX1605859) from [Vongsangnak et al 2016](https://peerj.com/articles/2534/), 44233 transcripts, average 1023bp
* [Glyptonus antarcticus](https://bitbucket.org/wrf/misc-transcriptomes/downloads/glyptonus_antarcticus_trinity.renamed.fasta.gz) : control sample [SRR4017485](https://www.ncbi.nlm.nih.gov/sra/?term=SRR4017485)
* [Sphaeroma terebrans](https://bitbucket.org/wrf/misc-transcriptomes/downloads/sphaeroma_terebrans_trinity.renamed.fasta.gz) : [SRR4436643](https://www.ncbi.nlm.nih.gov/sra/?term=SRR4436643) appears to be from [Han et al 2018](https://doi.org/10.1007/s13258-017-0618-4)

## onychophora ##
* [Peripatopsis capensis](https://bitbucket.org/wrf/misc-transcriptomes/downloads/peripatopsis_capensis_trinity.renamed.fasta.gz) : [SRR1145776](https://www.ncbi.nlm.nih.gov/sra/SRR1145776) from [Sharma et al 2014](https://doi.org/10.1093/molbev/msu235), 278204 transcripts, average 355bp

## loricifera ##
* [Armorloricus elegans](https://bitbucket.org/wrf/misc-transcriptomes/downloads/armorloricus_elegans_trinity.renamed.fasta.gz) : [SRR2131253](https://www.ncbi.nlm.nih.gov/sra/SRX1120677), 64684 transcripts, average 425bp

## cycliophora ##
* [Symbion pandora](https://bitbucket.org/wrf/misc-transcriptomes/downloads/symbion_pandora_trinity.renamed.fasta.gz) : [SRR3102772](https://www.ncbi.nlm.nih.gov/sra/SRX1531719) from [Neves et al 2017](https://link.springer.com/article/10.1007/s13127-016-0315-1), 102990 transcripts, average 693bp

`~/trinityrnaseq-Trinity-v2.4.0/Trinity --seqType fq --max_memory 32G --left Symbion_pandora_SRR3102772_1.fastq.gz --right Symbion_pandora_SRR3102772_2.fastq.gz --CPU 6 --full_cleanup --trimmomatic --output symbion_pandora_trinity > symbion_pandora_SRR3102772_trinity.log`

# assemblies on NCBI #
As of 2018-04-05, approximately 3000 assemblies are available on the [NCBI Trace Archive](https://www.ncbi.nlm.nih.gov/Traces/wgs/). These are mostly arthropods (almost 1000 hexapods). I have put a formatted table with Kingdom-Phylum-Class [on my github](https://github.com/wrf/misc-analyses/tree/master/taxonomy_database).

![sra_trace_species_list_2018-04-05.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/taxonomy_database/sra_trace_species_list_2018-04-05.png)
